#!/usr/bin/ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

doc = Nokogiri::HTML(open("https://officecdn.microsoft.com/pr/C1297A47-86C4-4C1F-97FA-950631F94777/OfficeMac/0409MSOf14.xml",
	  "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36"))

updates = []

doc.xpath('//string').each do |link|
    if link.content.match(/.*.dmg/)
        next if link.content.match(/1410Update/);
        next if link.content.match(/1423Update/);  # The 14.2.3 update isn't a flat-pack
        updates << link.content
    end
end

updates.each do |update|
    system("./clean.sh")
    system("./build.sh", update)
end
